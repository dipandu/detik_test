<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detik_article extends Model
{
    //
    public $timestamps = true;
    protected $table = 'detik_article';
}
