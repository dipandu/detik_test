<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\DB;
use Illuminate\Routing\UrlGenerator;
use Validator;
use App\Models\Detik_article as DetikDB;

class DetikController extends Controller
{
  public function index()
  {
    $articles = DetikDB::get();

    if(!$articles){
      $data = [];
    }else{
      $data = $articles->toArray();
    }

    return view('detik/dashboard', ['articles' => $data]);
  }

  public function saveArticle(Request $request)
  {
    // dd($request->input());
    $validation_rules = [
      'title' => 'required',
      'shortDescription' => 'required|max:150',
      'content' => 'required',
      'writer' => 'required'
    ];

    $validator = Validator::make($request->input(), $validation_rules);

    if($request->input('id') == null){
      if($validator->passes()){
        $article = new DetikDB;
        $article->title = $request->input('title');
        $article->short_content = $request->input('shortDescription');
        $article->full_content = $request->input('content');
        $article->writer = $request->input('writer');

        if($article->save()){
          $article_slug = str_slug($article->id.' '.$request->input('title'), '-');
          $article->where('id', $article->id)->update([
            'slug' => $article_slug
          ]);

          return [
            'status' => true
          ];
        }else{
          return [
            'status' => false,
            'messages' => [
              'insert_error' => 'Failed to save the article, Please contact administrator!'
            ]
          ];
        }
      }else{
        return [
          'status' => false,
          'messages' => $validator->messages()
        ];
      }
    }else{
      if($validator->passes()){
        $article = DetikDB::find($request->input('id'));
        $article->title = $request->input('title');
        $article->short_content = $request->input('shortDescription');
        $article->full_content = $request->input('content');
        $article->writer = $request->input('writer');

        if($article->save()){
          $article_slug = str_slug($request->input('id').' '.$request->input('title'), '-');
          $article->where('id', $request->input('id'))->update([
            'slug' => $article_slug
          ]);

          return [
            'status' => true
          ];
        }else{
          return [
            'status' => false,
            'messages' => [
              'insert_error' => 'Failed to save the article, Please contact administrator!'
            ]
          ];
        }
      }else{
        return [
          'status' => false,
          'messages' => [
            'insert_error' => 'Failed to save the article, Please contact administrator!'
          ]
        ];
      }
    }
  }

  public function deleteArticle(Request $request)
  {
    $article = DetikDB::find($request->input('id'));

    if($article->delete()){
      return [
        'status' => true
      ];
    }else{
      return [
        'status' => false,
        'messages' => [
          'delete_error' => 'Failed to delete article'
        ]
      ];
    }
  }

  public function getArticle(Request $request)
  {
    if(!$request->has('id')){
      abort(404);
    }

    $article = DetikDB::find($request->input('id'))
    ->first();

    if($article){
      $data = $article->toArray();
      return [
        'status' => true,
        $data
      ];
    }else{
      abort(404);
    }
  }

  public function viewArticle($slug){
    if(empty($slug)){
      abort(404);
    }

    $exp_slug = explode('-', $slug);

    $article = DetikDB::find($exp_slug[0]);
    if($article->first()){
      return view('detik/view', $article->toArray());
    }else{
      abort(404);
    }

  }
}
