@php
  // dd($articles);
@endphp
@extends('detik.master')
@section('content')
  <div class="container py-5">
    <div class="row">
      <div class="col-12">
        <h4>{{$title}}</h4>
        Written By : <i>{{$writer}}</i><br>
        Posted On : <i>{{substr($created_at, 0, 10)}}</i><br>
        <b>Short Content</b><br />
        <p class="mb-3">{{$short_content}}</p>
        <b>Full Content</b><br />
        <p>{{$full_content}}</p>
      </div>
    </div>
  </div>
@endsection

@section('custom_script')
<script type="text/javascript">
  $('#createBtn').click(function(e){
    $('#editorModal').modal('show');
  });

  $('#saveBtn').click(function(e){
    $.ajax({
      type:"POST",
      url:'{{url('detik/save')}}',
      data:JSON.stringify({
        '_token':'{{csrf_token()}}',
        'title':$('#title').val(),
        'shortDescription':$('#shortDescription').val(),
        'content':$('#content').val(),
        'writer':$('#writer').val(),
        'id':$('#id').val()
      }),
      dataType:"json",
      success:function(rsp){
        if(rsp.status){
          location.reload();
        }else{
          var errorMessage = '';
          $.each(rsp.messages, function(i, v){
            errorMessage+=v+'<br / />';
          });
          $('.alert').html(errorMessage);
          $('.alert').show();
        }
      },
      statusCode: {
        404: function() {
          alert("Unable to fetch data! Please contact the administrator.");
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: 'application/json',
      processData: false
    });
  });

  $('.btn-delete').click(function(e){
    var id = $(this).data('id');
    // return console.log(id);
    $.ajax({
      type:"POST",
      url:'{{url('detik/delete')}}',
      data:JSON.stringify({
        '_token':'{{csrf_token()}}',
        'id':id
      }),
      dataType:"json",
      success:function(rsp){
        if(rsp.status){
          location.reload();
        }else{
          console.log(rsp.messages);
        }
      },
      statusCode: {
        404: function() {
          alert("Unable to fetch data! Please contact the administrator.");
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: 'application/json',
      processData: false
    });
  });

  $('.btn-edit').click(function(e){
    var id = $(this).data('id');
    // return console.log(id);
    $.ajax({
      type:"POST",
      url:'{{url('detik/get')}}',
      data:JSON.stringify({
        '_token':'{{csrf_token()}}',
        'id':id
      }),
      dataType:"json",
      success:function(rsp){
        if(rsp.status){
          $('#id').val(rsp[0].id);
          $('#title').val(rsp[0].title);
          $('#shortDescription').html(rsp[0].short_content);
          $('#content').html(rsp[0].full_content);
          $('#writer').val(rsp[0].writer);
          $('#editorModal').modal('show');
        }else{
          console.log(rsp.messages);
        }
      },
      statusCode: {
        404: function() {
          alert("Unable to fetch data! Please contact the administrator.");
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: 'application/json',
      processData: false
    });
  });

  $('#editorModal').on('hide.bs.modal', function(e){
    location.reload();
  });
</script>
@endsection
