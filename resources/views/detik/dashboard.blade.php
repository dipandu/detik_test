@php
  // dd($articles);
@endphp
@extends('detik.master')
@section('custom_style')
  <style media="screen">
    .modal-lg {
        max-width: 80% !important;
    }
  </style>
@endsection
@section('content')
  <div class="container py-5">
    <div class="row mb-2">
      <div class="col-12">
        <button type="button" name="createBtn" id="createBtn" class="btn btn-primary float-right">Create New Article</button>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered table-hover">
          <thead>
            <th>Title</th>
            <th>Creator</th>
            <th>Created</th>
            <th>Action</th>
          </thead>
          <tbody>
            @if (!empty($articles))
              @foreach ($articles as $article)
                <tr>
                  <td><a target="_blank" href="{{url('detiktest/view/'.$article['slug'])}}">{{$article['title']}}</a></td>
                  <td>{{$article['writer']}}</td>
                  <td>{{substr($article['created_at'], 0, 10)}}</td>
                  <td class="text-center">
                    <button class="btn btn-danger btn-delete" data-id="{{$article['id']}}">Delete</button>
                    <button class="btn btn-warning btn-edit" data-id="{{$article['id']}}">Edit</button>
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td class="text-center" colspan="4"><b>No Data</b></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="editorModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            <div class="alert alert-danger" role="alert" style="display: none;">
            </div>
          </div>
          <div class="col-12">
            <input type="text" name="title" id="title" class="form-control" placeholder="Title">
            <input type="hidden" name="id" id="id">
          </div>
          <div class="col-12 mt-3">
            <textarea name="shortDescription" id="shortDescription" rows="4" placeholder="Short Description Max 150 Characters." class="form-control"></textarea>
          </div>
          <div class="col-12 mt-3">
            <textarea name="content" id="content" rows="6" placeholder="Content" class="form-control"></textarea>
          </div>
          <div class="col-12 mt-3">
            <select class="form-control" name="writer" id="writer">
              <option selected disabled>Writers</option>
              @foreach (array('Alex', 'Pandu', 'Hong', 'Seonsik') as $writer)
                <option value="{{$writer}}">{{$writer}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="saveBtn">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_script')
<script type="text/javascript">
  $('#createBtn').click(function(e){
    $('#editorModal').modal('show');
  });

  $('#saveBtn').click(function(e){
    $.ajax({
      type:"POST",
      url:'{{url('detiktest/save')}}',
      data:JSON.stringify({
        '_token':'{{csrf_token()}}',
        'title':$('#title').val(),
        'shortDescription':$('#shortDescription').val(),
        'content':$('#content').val(),
        'writer':$('#writer').val(),
        'id':$('#id').val()
      }),
      dataType:"json",
      success:function(rsp){
        if(rsp.status){
          location.reload();
        }else{
          var errorMessage = '';
          $.each(rsp.messages, function(i, v){
            errorMessage+=v+'<br / />';
          });
          $('.alert').html(errorMessage);
          $('.alert').show();
        }
      },
      statusCode: {
        404: function() {
          alert("Unable to fetch data! Please contact the administrator.");
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: 'application/json',
      processData: false
    });
  });

  $('.btn-delete').click(function(e){
    var id = $(this).data('id');
    // return console.log(id);
    if(confirm("Are you sure to delete?") == true){
      $.ajax({
        type:"POST",
        url:'{{url('detiktest/delete')}}',
        data:JSON.stringify({
          '_token':'{{csrf_token()}}',
          'id':id
        }),
        dataType:"json",
        success:function(rsp){
          if(rsp.status){
            location.reload();
          }else{
            console.log(rsp.messages);
          }
        },
        statusCode: {
          404: function() {
            alert("Unable to fetch data! Please contact the administrator.");
            $(document.body).css({'cursor' : 'default'});
          }
        },
        cache: false,
        contentType: 'application/json',
        processData: false
      });
    }
  });

  $('.btn-edit').click(function(e){
    var id = $(this).data('id');
    // return console.log(id);
    $.ajax({
      type:"POST",
      url:'{{url('detiktest/get')}}',
      data:JSON.stringify({
        '_token':'{{csrf_token()}}',
        'id':id
      }),
      dataType:"json",
      success:function(rsp){
        if(rsp.status){
          $('#id').val(rsp[0].id);
          $('#title').val(rsp[0].title);
          $('#shortDescription').html(rsp[0].short_content);
          $('#content').html(rsp[0].full_content);
          $('#writer').val(rsp[0].writer);
          $('#editorModal').modal('show');
        }else{
          console.log(rsp.messages);
        }
      },
      statusCode: {
        404: function() {
          alert("Unable to fetch data! Please contact the administrator.");
          $(document.body).css({'cursor' : 'default'});
        }
      },
      cache: false,
      contentType: 'application/json',
      processData: false
    });
  });

  $('#editorModal').on('hide.bs.modal', function(e){
    location.reload();
  });
</script>
@endsection
